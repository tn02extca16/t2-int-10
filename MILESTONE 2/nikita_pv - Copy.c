#include<stdio.h>
int power(int x , unsigned int nperiods)

    {
	  if(nperiods == 0)
		return (1);
	  else if(nperiods%2 == 0)
		return power(x , nperiods/2)*power(x , nperiods/2);
	  else
		return x*power(x , nperiods/2)*power(x , nperiods/2);
	}

int main()
    {
		unsigned int nperiods;
		double FV,rate ,PV;
		printf("Enter value of an future investment:\n");
		scanf("%lf",&FV);
		printf("Enter the value of rate:\n");
		scanf("%lf",&rate);
		printf("Enter the value of nperiods:\n");
		scanf("%d",&nperiods);
		PV = FV /power((1+rate),nperiods);
		printf("Present Value of an investment:%lf",PV);
		return(0);
    }


