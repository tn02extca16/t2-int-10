#include <stdio.h>

unsigned long nPr(unsigned short n, unsigned short r);
unsigned long factorial(unsigned short int);

int main() {
	unsigned short int n, r;

	printf("Enter n: ");
	scanf("%hu", &n);


	printf("Enter r: ");
	scanf("%hu", &r);

	printf("%huP%hu: %lu", n, r, nPr(n,r));
	printf("\n");

	return 0;
}

unsigned long nPr(unsigned short n, unsigned short r) {
	return factorial(n)/factorial(n-r);
}

unsigned long factorial(unsigned short n) {
	unsigned long f;

	if(n == 0 || n == 1)
		return 1;
	else
		f = n * factorial(n-1);

	return f;
}
