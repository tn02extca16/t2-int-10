#include <stdio.h>
#include <math.h>

int main()
{
    int i,n,t;
    float p,fv,r;

    puts("Compound Interest Calculator");

    /* gather data */
    printf("Initial balance: $");
    scanf("%f",&p);
    printf("Interest rate percentage: ");
    scanf("%d",&i);
    r = (float)i/100;
    printf("Compunding times per year: ");
    scanf("%d",&n);
    printf("Term in years: ");
    scanf("%d",&t);

    /* calculate */
    fv = p * pow( ( 1 + (r/n) ), t*n );
    printf("Future value is %.2f\n",fv);

    return(0);
}
