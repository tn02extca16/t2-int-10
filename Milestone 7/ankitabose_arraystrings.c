#include<stdio.h>
#include <stdlib.h>
#include<string.h>
int add_str(char str[10][50],int n,int count);
int remove_str(char str[10][50],int n,int count);
int search(char str[10][50],int n);
int str_copy(char *str1, char *str2);
int ascending(char str[10][50],int n);
int descending(char str[10][50],int n);
int printarray(char str[10][50],int n);
int main()
{
	char str[10][50];
	int i,j,n,choice,count;
	printf("Enter the number of elements :");
	scanf("%d",&n);
	printf("Enter the elements :\n");
	for(i=0;i<n;i++)
	{
		scanf("%s",&str[i]);
	}
	while(choice!=6)
	{
	printf("\n 1.Add an element to the Array \n 2.Search if an element exists in the array\n 3.Remove an element from the array\n 4. Sort the array\n 5. Print all elements in the array\n 6. Exit\n Enter your Option: ");
	scanf("%d",&choice);
		switch(choice)
		{
			case 1:add_str(str,n,count);

					if(count=count+1)
					{
						n=n+1;

					}
					break;
			case 2:search(str,n);
					break;
			case 3:remove_str(str,n,count);
					if(count=count+1)
					{
						n=n-1;

					}
					break;
			case 4:printf("\nEnter your choice:\n 1.Sort in Ascending order\n 2.Sort in Descending order\n");
					scanf("%d",&choice);
					switch(choice)
					{
						case 1:ascending(str,n);
								break;
						case 2:descending(str,n);
								break;
						default:printf("\nInvalid choice\n");
								break;
					}
					break;
			case 5:printarray(str,n);
					break;
			case  6:exit(0);
			default: printf("\nInvalid choice\n");
					break;
		}
}
}

int add_str(char str[10][50],int n,int count)
{
	char new[50];
	int i,j;
	count=0;
	printf("Enter the element to be added :\n");
	scanf("%s",&new);
	for(i=0;i<n;i++)
	{
		if(strcmp(str[i],new)!=0)
		{
			if(i==n-1)
			{
				strcpy(str[i+1],new);
				printf("Element added successfully\n");
				printf("New array is :\n");
				for(i=0;i<n+1;i++)
				{
					printf("%s\t",str[i]);
					count++;
				}
			}
		}
		else
		{
			printf("Element already added. Please try again.\n");
			break;
		}
	}
}

int search(char str[10][50],int n)
{
	char s[50];
	int i,j,success=0;
	printf("Enter the string to be found: ");
	scanf("%s",&s);
	for(i=0;i<n;i++)
	{
		if(strcmp(str[i],s)==0)
		{
			printf("Found %s at index %d ",s,i+1);
			break;
		}
	}
	if(i==n)
	{
		printf("%s not found in array");
	}
}

int remove_str(char str[10][50],int n,int count)
{
	char s[50];
	int i,j=0;
	count=0;
	printf("Enter the string to be removed: ");
	scanf("%s",&s);
	for(i=0;i<n;i++)
	{
		if(strcmp(str[i],s)==0)
		{
			while(i<n-1)
			{
				str_copy(str[i],str[i+1]);
				i++;
			}
			printf("Element Removed.\nNew array is :\n");
			for(i=0;i<n-1;i++)
			{
				printf("%s\t",str[i]);
				count++;
			}

		}
		else if(i==n-1)
		{
			printf("%s not removed\n",s);
		}
	}
}

int ascending(char str[10][50],int n)
{
	char temp[50];
	int i,j;
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(strcmp(str[i],str[j])>0)
			{
				str_copy(temp,str[i]);
				str_copy(str[i],str[j]);
				str_copy(str[j],temp);
			}
		}
	}
	printf("The array sorted in ascending order is:\n");
	for(i=0;i<n;i++)
	{
		printf("%s\t",str[i]);
	}
}

int descending(char str[10][50],int n)
{
	char temp[50];
	int i,j;
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(strcmp(str[i],str[j])<0)
			{
				str_copy(temp,str[i]);
				str_copy(str[i],str[j]);
				str_copy(str[j],temp);
			}
		}
	}
	printf("The array sorted in descending order is:\n");
	for(i=0;i<n;i++)
	{
		printf("%s\t",str[i]);
	}
}

int str_copy(char *str1, char *str2)
{
	while(*str2 !='\0')
	{
		*str1=*str2;
		str2++;
		str1++;
	}
	*str1='\0';
}

int printarray(char str[10][50],int n)
{
	int i;
	printf("The array is :\n");
	for(i=0;i<n;i++)
	{
	printf("%s\t",str[i]);
	}
}
