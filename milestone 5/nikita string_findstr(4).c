#include<stdio.h>
int str_copy(char *str1,char *str2)
{
	int i = -1;
	while(str2[++i] != '\0')
	{
		str1[i] = str2[i];
	}
	return 0;
}
int main()
{
	char arr1[] = "Hello";
	char arr2[] = "World";
	str_copy(arr1,arr2);
	printf("Array 1 is %s and array 2 is %s",arr1,arr2);
}
