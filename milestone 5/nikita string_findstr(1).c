#include<stdio.h>
int str_find_char(char *str1,char *str2)
{
	int i =0,j = -1;
	while(str1[i++] != '\0')
	{
		if(str1[i] == *str2)
		{
			while(str2[++j] != '\0')
			{
				if(str2[j] != str1[i + j])
					break;
			}
			if(str2[j] == '\0')
				return i;
		}
	}
	return -1;
}
int main()
{
	char arr1[] = "Hello World!",a[10];
	printf("Enter character you want to find.\n");
	scanf("%s",&a);

	printf("String find function result is %d",str_find_char(arr1,a));
	return 0;
}
