#include<stdio.h>
int str_find_char(char *str,char *ch)
{
	int i =0;
	while(str[i++] != '\0')
	{
		if(str[i] == *ch)
		{
			return i + 1;
		}
	}
	return -1;
}
int main()
{
	char arr1[] = "Hello World!",a;
	printf("Enter character you want to find.\n");
	scanf("%c",&a);

	printf("String find function result is %d",str_find_char(arr1,&a));
	return 0;
}
