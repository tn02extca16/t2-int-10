#include<stdio.h>
int str_compare(char *str1,char *str2)
{
	int pts1 = 0,pts2 = 0,i = 0;

	while(str1[i++] != '\0')
	{
		pts1 = pts1 + (int)str1[i];
	}
	i = 0;
	while(str2[i++] != '\0')
	{
		pts2 = pts2 + (int)str2[i];
	}
	if(pts1 == pts2)
	{
		return 0;
	}
	return pts1 > pts2? 1:-1;
}
int main()
{
	char arr1[] = "World",arr2[] = "World";

	printf("String compare result is %d",str_compare(arr1,arr2));

	return 0;
}
